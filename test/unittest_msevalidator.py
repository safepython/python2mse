import imp
import os, os.path as osp
from logilab.common.testlib import InnerTest, TestCase

from python2mse.mseparser import Parser, mse_grammar

DATA = osp.join(osp.dirname(__file__), 'data', 'msevalidator')


def msevalidator(rule='Root'):
    """Just check the syntax is correct"""
    return Parser(mse_grammar, rule)


class MSEValidatorTC(TestCase):
    def check_validator_ok(self, pyfile):
        datam = imp.load_source('data', pyfile)
        self.assertTrue(hasattr(datam, 'mse_ok'))

        for rule, mse_snippets in getattr(datam, "mse_ok", []):
            parser = msevalidator(rule)
            for mse_snippet in mse_snippets:
                success, children, next = parser.parse(mse_snippet)
                self.assertEqual(1, success)

    def check_validator_nok(self, pyfile):
        datam = imp.load_source('data', pyfile)
        self.assertTrue(hasattr(datam, 'mse_nok'))

        for rule, mse_snippets in getattr(datam, "mse_nok", []):
            parser = msevalidator(rule)
            for mse_snippet in mse_snippets:
                success, children, next = parser.parse(mse_snippet)
                self.assertEqual(0, success)


for dirpath, dirnames, filenames in os.walk(DATA):
    testbasename = '_'.join(dirpath[len(DATA)+1:].split('/'))
    for pyfile in filenames:
        if pyfile.endswith('.py'):
            testname = '%s_%s' % (testbasename, pyfile[:-3])
            pyfile = osp.join(dirpath, pyfile)

            def test_msevalidator_ok(self, pyfile=pyfile):
                self.check_validator_ok(pyfile)
            # make sure the test name is correct so pytest can filter
            # it out correctly
            test_msevalidator_ok.__name__ = 'test%s_ok' % testname
            setattr(MSEValidatorTC, 'test%s_ok' % testname, test_msevalidator_ok)

            def test_msevalidator_nok(self, pyfile=pyfile):
                self.check_validator_nok(pyfile)
            # make sure the test name is correct so pytest can filter
            # it out correctly
            test_msevalidator_nok.__name__ = 'test%s_nok' % testname
            setattr(MSEValidatorTC, 'test%s_nok' % testname, test_msevalidator_nok)


if __name__ == "__main__":
    from logilab.common.testlib import unittest_main
    unittest_main()
