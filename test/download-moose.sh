#! /usr/bin/env bash

MOOSE=5_0

wget -O boa.zip https://ci.inria.fr/rmod/job/Boa/OS=linux/lastSuccessfulBuild/artifact/boa_linux.zip

# extract the .image and .changes files into the current directory
unzip -j boa.zip boa/moose_suite_${MOOSE}/shared/moose_suite_${MOOSE}.image boa/moose_suite_${MOOSE}/shared/moose_suite_${MOOSE}.changes

mv moose_suite_${MOOSE}.image moose.image
mv moose_suite_${MOOSE}.changes moose.changes

rm -f boa.zip
