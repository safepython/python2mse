# -*- coding: utf-8 -*-
import string

mse_ok = [('STRING', [
            "''",
            "'ceci'",
            "'ceci est une string'",
            "'ceci''sont''des''string'",
            "'test/data/testMethodInvocation.py'",
            "'method1(self)'",
            """'some "string"'""",
            "'éà€'",
            ]),
          ]

mse_nok = [('Root', ["((toto 'ce'ci'))","((toto 'c'e'ci'))"]),
           ]


