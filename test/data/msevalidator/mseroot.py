mse_ok = [('Root', [
        """()""",
        """( )""",
        """((FAMIX.PyModule))""",
        """((FAMIX.PyModule(id:1)))""",
        """((FAMIX.PyModule (id:1)))""",
        """((FAMIX.PyModule (id:1) ) )""",
        """((FAMIX.PyModule(id:1)(attribute(ref:5)) ) )""",
        """((FAMIX.PyModule(id:1)
            (attribute(ref:5))
        ) )""",
        """((FAMIX.PyModule(id:1)
            (attribute(ref:5))
            (attribute(ref:5))
        ) )""",
        """(
        (FAMIX.PyFileAnchor (id: 1)
        (element (ref: 5))
        (endLine 4)
        (startLine 0)))""",
        """(
        (FAMIX.PyFileAnchor (id: 1)
        (element (ref: 5))
        (endLine 4)
        (fileName 'test/data/testMethodInvocation.py')
        (startLine 0)))""",
        """(
    (FAMIX.PyFileAnchor (id: 1)
        (element (ref: 5))
        (endLine 4)
        (fileName 'test/data/testMethodInvocation.py')
        (startLine 0))

    (FAMIX.PyClass (id: 3)
        (name 'object')
        (isStub true)))""",

    """((FAMIX.Comment (id: 12)
         (content 'Constant-time string comparison.\n\n    :params provided: the first string\n    :params known: the second string\n\n    :return: True if the strings are equal.\n\n    This function takes two strings and compares them.  It is intended to be\n    used when doing a comparison for authentication purposes to help guard\n    against timing attacks.  When using the function for this purpose, always\n    provide the user-provided password as the first argument.  The time this\n    function will take is always a factor of the length of this string.\n    ')
         (container (ref: 8))
    ))""",
    """(
    (FAMIX.PyFileAnchor (id: 1)
        (element (ref: 5))
        (endLine 4)
        (fileName 'test/data/testMethodInvocation.py')
        (startLine 0))

    (FAMIX.PyClass (id: 3)
        (name 'object')
        (isStub true))

    (FAMIX.PyClass (id: 4)
        (name 'BankAccount')
        (container (ref: 5))
        (isStub false)
        (sourceAnchor (ref: 6)))

    (FAMIX.PyModule (id: 5)
        (name 'testMethodDefinition')
        (isStub false)
        (sourceAnchor (ref: 1)))

    (FAMIX.PyFileAnchor (id: 6)
        (element (ref: 4))
        (endLine 4)
        (fileName 'test/data/testMethodInvocation.py')
        (startLine 2))

    (FAMIX.PyMethod (id: 7)
        (name 'method1')
        (cyclomaticComplexity 1)
        (numberOfStatements 1)
        (numberOfParameters 1)
        (parentType (ref: 4))
        (signature 'method1(self)')
        (sourceAnchor (ref: 8)))

    (FAMIX.PyFileAnchor (id: 8)
        (element (ref: 7))
        (endLine 4)
        (fileName 'test/data/testMethodInvocation.py')
        (startLine 3))

    (FAMIX.Inheritance (id: 9)
        (subclass (ref: 4))
        (superclass (ref: 3)))

    (FAMIX.PyParameter (id: 10)
            (name 'self')
            (parentBehaviouralEntity (ref: 7)))

    (FAMIX.PyParameter (id: 11)
        (name 'initial_balance')
            (parentBehaviouralEntity (ref: 7)))

    (FAMIX.PyMethod (id: 12)
        (name 'method2')
        (cyclomaticComplexity 1)
        (numberOfStatements 1)
        (numberOfParameters 1)
        (parentType (ref: 4))
        (signature 'method2(self)')
        (sourceAnchor (ref: 8)))

    (FAMIX.PyFileAnchor (id: 13)
        (element (ref: 12))
        (endLine 7)
        (fileName 'test/data/testMethodInvocation.py')
        (startLine 6))

    (FAMIX.Invocation (id: 14)
        (candidates (ref: 12))
        (sender (ref: 7))
        (signature 'function1()'))

)
""",
        ]),
               ]

mse_nok = []

