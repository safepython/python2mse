'''
    @copyright: 
        BUZZY software
        ThereSIS Research Center
        Copyright 2010 Thales Services SAS - All rights reserved 
    @author: 
        Cedric BRANDILY

'''
# pylint: disable=W0221,W0703, C0111
# TODO document module
from Sharehive import HiveError
from Toolhive.XtendedXmlrpcServer import XtendedWrapper
from xmlrpclib import ServerProxy
import threading

def __callAsync(callFunction, callbackFunction=None, *args):
    '''
    perform an async call
    @param callFunction: function to call
    @type callFunction: function
    @param callbackFunction: callback function
    @type callbackFunction: function
    @param *args: function args
    @type *args: list
    '''
    def callSync(*args):
        '''function to call asynchronously'''
        result = callFunction(*args)
        if callbackFunction is not None:
            try:
                callbackFunction(result)
            finally:
                pass
    threading.Thread(target=callSync, args=args).start()

class HiveXmlrpcWrapper(XtendedWrapper):
    def syncWrap(self, func, dictionary):
        try:
            try:
                ticketId = dictionary['ticketId']
                requestId = dictionary['requestId']
                del dictionary['ticketId']
                del dictionary['requestId']
                if len(dictionary) > 0:
                    res = func(**dictionary)
                else:
                    res = func()
                return dict(success=True, ticketId=ticketId,
                        requestId=requestId, data=res)
            except TypeError, e:
                raise HiveError.HiveTypeError(e)
        except Exception, e:
            try:
                HiveError.printTrace("Exception in function: %s(%s)"\
                        % (func.__name__, dictionary), e)
                return e.getReturn(ticketId=ticketId, requestId=requestId)
            except:
                HiveError.printTrace("Unknown error", e)
                return HiveError.UnknownError(str(e)).getReturn(
                        ticketId=ticketId, requestId=requestId)
            
    def asyncWrap(self, func, defaultCallbackFunction, dictionary):
        callbackUri = dictionary['rpcCallbackUri']
        callbackProxy = ServerProxy(callbackUri)
        del dictionary['rpcCallbackUri']
        if 'rpcCallbackFunction' in dictionary:
            callbackFunction = dictionary['rpcCallbackFunction']
            del dictionary['rpcCallbackFunction']
        else: 
            callbackFunction = defaultCallbackFunction
        __callAsync(self.syncWrap, getattr(callbackProxy,
                callbackFunction), (func, dictionary))
        return True
