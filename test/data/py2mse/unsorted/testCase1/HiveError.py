'''
    @copyright: 
        BUZZY software
        ThereSIS Research Center
        Copyright 2010 Thales Services SAS - All rights reserved
    @author: 
        Cedric BRANDILY
 
    This modules provides an error class generator
'''
import sys
import traceback
def buildMyErrorClass(errorId, errorClassName, errorDetail):
    '''
    generate an error class    
    @param errorId: error class id
    @type errorId: int
    @param errorDetail: error class description
    @type errorDetail: str
    '''

    class MyError(Exception):
        '''Generated class'''
        def __init__(self, errorInfo=""):            
            Exception.__init__(self)
            self.errorInfo = errorInfo
        def getReturn(self, **kwargs):
            '''build error return'''
            d = dict(errorId=errorId, errorDetail=errorDetail, 
                     errorInfo=self.errorInfo)
            d.update(kwargs)
            return d
    MyError.__name__ = errorClassName
    return MyError

ConfigError = buildMyErrorClass(991, 
        "ConfigError",
        "Config error")
HiveTypeError = buildMyErrorClass(997, 
        "HiveTypeError",
        "Type error")
UnknownError = buildMyErrorClass(998, 
        "UnknownError",
        "Unknown error")
UnimplementedError = buildMyErrorClass(999, 
        "UnimplementedError",
        "Unimplemented error")



InvalidInputError =  buildMyErrorClass(1, 
        "InvalidInputError",
        "Invalid input")
UnknownKeyError =    buildMyErrorClass(2, 
        "UnknownKeyError",
        "Unknown key")
DuplicateKeyError =  buildMyErrorClass(3, 
        "DuplicateKeyError",
        "Duplicate key")
ResourceLackError =  buildMyErrorClass(4, 
        "ResourceLackError",
        "No free resource")
UnknownActionError = buildMyErrorClass(5, 
        "UnknownActionError",
        "Unknown action")
UnknownStatusError = buildMyErrorClass(6, 
        "UnknownStatusError",
        "Unknown status")
UnauthorizedActionError = buildMyErrorClass(7, 
        "UnauthorizedActionError",
        "Unauthorized action")
NoDeployableTemplateError = buildMyErrorClass(8, 
        "NoDeployableTemplateError",
        "Not a deployable template")
MigrationError = buildMyErrorClass(9, 
        "MigrationError",
        "Migration did not succeed")
UnknownObjectError = buildMyErrorClass(10, 
        "UnknownObject",
        "Unknown object")
NoResourceAllocated = buildMyErrorClass(11, 
        "NoResourceAllocated",
        "No resource allocated")


def printTrace(title, e):
    '''print trace'''
    print title
    print "type: %s" % str(type(e))
    print '-' * 100
    traceback.print_exc(file=sys.stdout)
    print '-' * 100
