'''
    @copyright: 
        BUZZY software
        ThereSIS Research Center
        Copyright 2010 Thales Services SAS - All rights reserved
    @author: 
        Cedric BRANDILY
'''
import random
import re
import unittest


class MyTestCase(unittest.TestCase):
    '''
    Specializes unittest.TestCase
    Add new assertions
    Add a service locator
    '''
    
    def __init__(self, methodName='dummy'):
        unittest.TestCase.__init__(self, methodName)
    
    def dummy(self):
        pass  
    
    def rand(self, min=1, max=10**10):
        return random.randint(min, max)
    
    def assertIdDict(self, obj, idName):
        self.assertIsInstance(obj, dict)
        self.assertTrue(idName in obj)
        self.assertIsInstance(obj[idName], int)
    
    def assertListResult(self, obj, idName):
        self.assertIsInstance(obj, list)
        for idDict in obj:
            self.assertIdDict(idDict, idName)
        
    
    def assertIsInstance(self, obj, cls, msg=None):
        ''' assertTrue(isinstance(obj,cls), msg) '''
        msg = msg or '%s is not an instance of %r' % (repr(obj), cls)
        self.assertTrue(isinstance(obj, cls), msg)
        
    def assertKeyValue(self, d, key, cls, msg=None):
        '''check if key is a key of d and its value type'''
        msg = msg or ""
        self.assertTrue(key in d,
                msg + " (key %s not found in dict %s)" % (key, d))
        self.assertIsInstance(d[key], cls,
                msg + " (value %s type != %s)" % (d[key], cls))        
        
    def assertIsDn(self, domainName, msg=None):
        '''assertTrue(isDn(domainName), msg)'''
        msg = msg or '%s is not a valid domainName' % repr(domainName)
        self.assertTrue(self.__isDn(domainName), msg)
        
    def assertIsFqdn(self, fqdn, msg=None):
        '''assertTrue(isFqdn(fqdn), msg)'''
        msg = msg or '%s is not a valid fqdn' % repr(fqdn)
        self.assertTrue(self.__isFqdn(fqdn), msg)
        
    def assertIsIp(self, ip, msg=None):
        '''assertTrue(isIp(ip), msg)'''
        msg = msg or '%s is not a valid ip' % repr(ip)
        self.assertTrue(self.__isIp(ip), msg)

        
    __dnRegexObj = re.compile("^[A-Za-z]([A-Za-z0-9-]*[A-Za-z0-9])?$")
    def __isDn(self, dn):
        '''
        returns True if dn is a correct domain name
        @param dn: domain name to test
        @type dn: str
        @see RFC952
        '''
        return (isinstance(dn, basestring) 
                and self.__dnRegexObj.match(dn) is not None)
    
    def __isFqdn(self, fqdn):
        '''
        returns True if fqdn is a correct fqdn
        @param fqdn: fqdn to test
        @type fqdn: str
        '''
        if not isinstance(fqdn, basestring):
            return False
        for dn in fqdn.split("."):
            if self.__dnRegexObj.match(dn) is None:
                return False
        return True
    
    __ipRegex = re.compile("^([0-9]{1,3}\.){3}[0-9]{1,3}$")
    def __isIp(self, ip):
        '''
        returns True if ip is a correct ip 
        @param ip: ip to test
        @type ip: str
        '''
        return (isinstance(ip, basestring) 
                and self.__ipRegex.match(ip) is not None)