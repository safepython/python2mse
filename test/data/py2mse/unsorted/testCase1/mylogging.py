'''
    @copyright: 
        BUZZY software
        ThereSIS Research Center
        Copyright 2010 Thales Services SAS - All rights reserved   
    @author: 
        Cedric BRANDILY
   
   This modules provides logging facilities 
'''

# pylint: disable=W0401,W0614
from logging import * #@UnusedWildImport
from logging.handlers import TimedRotatingFileHandler as _TRFHandler
import sys as _sys


__baseformatter = Formatter(
        fmt="%(asctime)s %(levelname)-5.5s %(name)s - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S")

def MyTimedRotatingHandler(
        filename="/var/log/hive.log",
        level=NOTSET,
        formatter=__baseformatter):
    ''' Build a rotating file handler '''
    handler = _TRFHandler(filename,
            when='D', interval=1, backupCount=5)
    handler.setFormatter(formatter)
    handler.setLevel(level)    
    return handler

class Stream2log(object):
    '''Redirect stream to a logger'''
    
    def __init__(self, stream, logfunc):
        '''
        Construct a stdout/stderr like object which allow to output to a logger 
        @param stream: stdout/stderr to package
        @param logfunc: log function
        '''
        self._stream = stream
        self._logfunc = logfunc
        self._buffer = []
        
    def write(self, s):
        '''
        write to log and stream
        @param s: string to output
        '''
        self._stream.write(s)
        if len(s) is 1 and ord(s) == 10:
            if len(self._buffer) > 0:
                self._logfunc("".join(self._buffer))
            self._buffer = []
        else:
            self._buffer.append(s)
    
    def flush(self):
        ''' flush stream ''' 
        return self._stream.flush()
        
    def fileno(self):
        ''' stream fileno '''
        return self._stream.fileno()

def configure(filenameBase='/var/log/hive.log', level=INFO, onlyConsole=False):
    ''' Configure logging policy '''
    rootLog = getLogger()
    rootLog.setLevel(level)
    consoleHandler = StreamHandler(_sys.stdout)
    consoleHandler.setFormatter(__baseformatter)
    rootLog.addHandler(consoleHandler)
    if not onlyConsole:
        rootLog.addHandler(MyTimedRotatingHandler(filenameBase))
        
        STDOUT = WARN+1
        addLevelName(STDOUT, "STDOUT")
        stdoutLog = getLogger("catched.stdout")
        stdoutHandler = MyTimedRotatingHandler(filenameBase, level=STDOUT)
        stdoutLog.addHandler(stdoutHandler)
        stdoutLog.propagate = False
        _sys.stdout = Stream2log(_sys.stdout, 
                lambda msg: stdoutLog.log(STDOUT, msg))
           
        STDERR = WARN+2
        addLevelName(STDERR, "STDERR")
        stderrLog = getLogger("catched.stderr")
        stderrHandler = MyTimedRotatingHandler(filenameBase, level=STDERR)
        stderrLog.addHandler(stderrHandler)
        stderrLog.propagate = False
        _sys.stderr = Stream2log(_sys.stderr, 
                lambda msg: stderrLog.log(STDERR, msg))