'''
    @copyright: 
        BUZZY software
        ThereSIS Research Center
        Copyright 2010 Thales Services SAS - All rights reserved  
    @author: 
        Cedric BRANDILY
 
    This module list all sections
'''
from Sharehive.Data.Service import Service

class Section(Service):
    ''' configuration sections '''
    # Shared sections
    ALL = 'DEFAULT'
    API = 'api'
    DB  = 'db'

    # Midhive sections
    MIDHIVE  = 'midhive'
    COLLECTD = 'collectd'

    # Keyhive sections
    KEYHIVE = 'keyhive'

    # Minidhive sections
    MINIDHIVE = 'minidhive'
    
    # Minidhive module sections
    BRIDGE = 'bridge'
    DHCP_RELAY  = 'dhcp-relay'
    DHCP_SERVER = 'dhcp-server'
    DNS_RELAY   = 'dns-relay'
    DNS_SERVER  = 'dns-server'
    FIREWALL = 'firewall'
    SWITCH = 'switch'
    VPN = 'vpn'