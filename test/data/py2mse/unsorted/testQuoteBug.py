class BankAccount(object):

    def method1(self, value="'a string'"):
        return value

    def method2(self):
        self.method1('thisIsAInsideString')

    def method3(self, a="children's song", b='\'yup\''):
        return "'" + a + "'" + b

    def method4(self):
        a = "children's song"
        b = self.method3(a)
        c = self.method3("'toto'")
        d = self.method3('"toto"')
