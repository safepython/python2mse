import sys
import os.path as osp
import StringIO

from recursivevisitor import ParentRecursiveVisitor
import astroid
from astroid.as_string import AsStringVisitor
from astroid.node_classes import Const, Arguments, Name, Call
from astroid.scoped_nodes import (ClassDef, Module, FunctionDef, Lambda,
                                  GenExpr, DictComp, SetComp)
from astroid.bases import Instance, BoundMethod, UnboundMethod
from astroid.util import YES
from astroid.exceptions import InferenceError, NotFoundError
import six


class DefinitionVisitor(ParentRecursiveVisitor):

    def __init__(self, out, refmgr, verbose=False):
        super(DefinitionVisitor, self).__init__(verbose)
        self.out = out
        self.ref_mgr = refmgr

    def close(self):
        "write MSE node for every unmanaged nodes as stub"
        missing = (set(self.ref_mgr.values()) - self.ref_mgr.managed)
        missing = [node for (node, ref) in self.ref_mgr.items() if ref in missing]
        for node in missing:
            func = getattr(self, "visit_" + node.__class__.__name__.lower(), None)
            if func:
                func(node)
            if not self.ref_mgr.is_managed(node):
                nodeid = self.ref_mgr.ensure_entity(node)
                self.write('(FAMIX.PyUnknown (id: %s)' % nodeid)
                self.write(')')
        # check no mseid has been given twice
        assert len(self.ref_mgr) == len(set(self.ref_mgr.values()))

    def write(self, bytes):
        self.out.write(bytes)
        self.out.write('\n')

    def leave_all(self, node):
        # pylint: disable=missing-docstring
        self._done.add(node)
        if self.verbose:
            sys.stderr.write("DONE {0}\n".format(node))
        if not self.ref_mgr.is_stub(node):
            # do only visit children if not stub
            if self.verbose:
                sys.stderr.write("  NOT STUB: VISITING CHILDREN\n")
            for child in node.get_children():
                if child not in self._done:
                    child.accept(self)

    # visitor methods
    def visit_module(self, node):
        self.currentModule = node
        self.importedNamed = node.name

        if self.ref_mgr.is_managed(node):
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        isstub = self.ref_mgr.is_stub(node)
        if not isstub:
            anchor = self.write_source_anchor(node)

        self.write("(FAMIX.PyModule (id: %s)" % nodeid)
        self.write("  (name '%s' )" % node.name)
        self.write('  (isPackage %s)' % str(node.package).lower())
        self.write('  (isStub %s)' % str(isstub).lower())
        if '.' in node.name:
            # we live in a package
            pkgname = node.name.rsplit('.', 1)[0]
            pkgref = self.ref_mgr.find_module(pkgname)
            if pkgref is not None:
                self.write('  (parentModule (ref: %s))' % pkgref)

        if not isstub and node.parent is not None:
            # XXX still meaningful?
            self.write('  (parent (ref: %s))' %
                       self.ref_mgr.ensure_entity(node.parent, False))
        if not isstub:
            self.write_source_anchorRef(anchor)
        self.write(')')
        if not isstub and self._has_comment(node):
            self.write_comment(node, nodeid)

    def visit_classdef(self, node):
        if self.ref_mgr.is_managed(node):
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        isstub = self.ref_mgr.is_stub(node)
        if not isstub:
            anchor = self.write_source_anchor(node)
        self.write('(FAMIX.PyClass (id: %s)' % nodeid)
        self.write("  (name '%s')" % node.name)
        self.write('  (isStub %s)' % str(isstub).lower())
        if not isstub:
            self.write('  (container (ref: %s))' %
                       self.ref_mgr.ensure_entity(node.parent, False))
            self.write_source_anchorRef(anchor)
        self.write(')')

        if not isstub:
            if (self._has_comment(node)):
                self.write_comment(node, nodeid)
            for nodename in node.bases:
                try:
                    base = list(nodename.infer())
                except InferenceError:
                    # baseclass is a stub
                    base = [ClassDef(nodename.as_string(), '')]
                    base[0].accept(self)

                for b in base:
                    if b is YES:
                        continue
                    baseid = self.ref_mgr.ensure_entity(b, False)
                    self.write('(FAMIX.Inheritance (id: %s)' % self.ref_mgr.ensure_entity((nodeid, baseid)))
                    self.write('  (subclass (ref: %s))' % nodeid)
                    self.write('  (superclass (ref: %s))' % baseid)
                    self.write(')')
            super(DefinitionVisitor, self).visit_classdef(node)

    def visit_arguments(self, node):
        scope = node.parent
        if node.vararg:
            id = self.ref_mgr.ensure_entity((node, 'vararg'))
            self.write('(FAMIX.PyParameter (id: %s )' % str(id))
            self.write('  (name %r)' % node.vararg)
            self.write('  (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(scope, False))
            self.write(')')
        if node.kwarg:
            id = self.ref_mgr.ensure_entity((node, 'kwarg'))
            self.write('(FAMIX.PyParameter (id: %s )' % str(id))
            self.write('  (name %r)' % node.kwarg)
            self.write('  (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(scope, False))
            self.write(')')

    def visit_functiondef(self, node):
        if self.ref_mgr.is_managed(node):
            return

        isstub = self.ref_mgr.is_stub(node)
        if isstub:
            self.write('(FAMIX.PyFunction (id: %s) ' % str(self.ref_mgr.ensure_entity(node)))
            self.write('  (container (ref: %s))' % str(self.ref_mgr.ensure_entity(node.root(), False)))
            self.write("  (name '%s')" % node.name)
            self.write("  (signature '%s(%s)')" % (node.name, node.args.accept(AsStringVisitor()).replace("'", "''")))

            self.write('  (isStub true)')
            self.write(')')
            return

        nodeid = self.ref_mgr.ensure_entity(node)
        anchor = self.write_source_anchor(node)
        if (node.is_method()):
            self.write('(FAMIX.PyMethod (id: %s)' % str(nodeid))
            self.write('  (parentType (ref: %s))' % str(self.ref_mgr.ensure_entity(node.parent, False)))
        else:
            self.write('(FAMIX.PyFunction (id: %s) ' % str(nodeid))
            #self.write('  (container (ref: %s))' % str(self.ref_mgr.ensure_entity(self.currentModule)))
            self.write('  (container (ref: %s))' % str(self.ref_mgr.ensure_entity(node.parent, False)))
        self.write("  (name '%s')" % node.name)
        self.write("  (signature '%s(%s)')" % (node.name, node.args.accept(AsStringVisitor()).replace("'", "''")))
        self.write_source_anchorRef(anchor)
        if node.args.args is not None:
            self.write("  (numberOfParameters %d)" % (len(node.args.args)))
        if hasattr(node, 'statements'):
            self.write("  (numberOfStatements %d)" % (node.statements))
        if hasattr(node, 'complexity'):
            self.write("  (cyclomaticComplexity %d)" % (node.complexity))

        self.write(')')
        if (not isstub and self._has_comment(node)):
            self.write_comment(node, nodeid)

    def visit_call(self, node):
        if self.ref_mgr.is_managed(node):
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        try:
            # when visiting a Call, we try to guess what is the called
            # function. This is done in infering node.func
            # we first build a list of possible functions
            funcs = []
            # node.func may itself be a Call, let's find the one
            # which is not a Call, on which we might get a chance to infer
            func = node
            while isinstance(func, Call):
                func = func.func
            for func in func.infer():
                if isinstance(func, (FunctionDef, BoundMethod)):
                    func = func.scope()
                if isinstance(func, Const):
                    for ifunc in func.infer():
                        if ifunc.callable():
                            funcs.append(ifunc)
                else:
                    funcs.append(func)

            # let's now build a list of possible candidates for
            # the called function, using the __call__ and __init__
            # special methods when needed
            candidates = []
            for func in funcs:
                if isinstance(func, ClassDef):
                    if '__init__' in func.locals:
                        candidates.extend(func.locals['__init__'])
                    else:
                        try:
                            # use the MRO
                            candidates.extend(func.getattr('__init__'))
                        except NotFoundError:
                            # occur when nobody in the MRO of the
                            # class defines no special __init__ method
                            pass
                elif isinstance(func, Instance):
                    try:
                        candidates.extend(func.getattr('__call__'))
                    except NotFoundError:
                        # XXX should do things here I guess
                        pass
                elif func is YES:
                    # could not infer the called function, create a stub function
                    if hasattr(node.func, 'name'):
                        funcname = node.func.name
                    elif hasattr(node.func, 'attrname'):
                        funcname = node.func.attrname
                    else:
                        # cannot say anuything about the called function,
                        # think for example of something like
                        # z = funclist[idx](args)
                        funcname = 'N/A'

                    stubnode = 'stub<{0}:{1}>'.format(nodeid, funcname)
                    self.write('(FAMIX.PyFunction (id: %s)' % str(self.ref_mgr.ensure_entity(stubnode)))
                    self.write("  (name '%s')" % funcname)
                    self.write('  (isStub true)')
                    self.write(')')
                    candidates.append(stubnode)
                else:
                    candidates.append(func)
        except InferenceError:
            candidates = [] # XXX can probably be a bit smarter than this

        self.write('(FAMIX.PyInvocation (id: %s)' % str(nodeid))
        if candidates:
            funcrefs = ['(ref: %s)' % self.ref_mgr.ensure_entity(func, False)
                        for func in candidates]
            self.write('  (candidates %s)' % ' '.join(funcrefs))

        self.write("  (signature '%s')" % node.as_string().replace("'", "''"))
        self.write('  (sender (ref: %s))' % self.ref_mgr.ensure_entity(node.scope(), False))
        try:
            receiver = node.func.infered()
            self.write('  (receiverCandidates {})'.format(' '.join('(ref: {})'.format(self.ref_mgr.ensure_entity(recv, False)) for recv in receiver)))
        except InferenceError:
            pass
        self.write(')')
        for func in candidates:
            if hasattr(func, 'callable') and func.callable() and func not in self._done:
                self.visit_functiondef(func)


    def visit_assign(self, node):
        for target in node.targets:
            if self.ref_mgr.is_managed((target, node)):
                continue

    def visit_assignattr(self, node):
        if self.ref_mgr.get_declaration_entity(node) is not node:
            # attribute declaration already managed
            return
        nodeid = self.ref_mgr.ensure_entity(node, managed=False)
        if self.ref_mgr.is_managed(self.ref_mgr.get_entity(nodeid)):
            return
        self.write('(FAMIX.Attribute (id: %s)' % nodeid)
        self.ref_mgr.managed.add(nodeid)

        self.write("  (name '%s')" % node.attrname)
        try:
            instance = node.expr.infer().next()
        except InferenceError:
            self.write("  (isStub true)")
        else:
            clsvar = not isinstance(instance, Instance)
            self.write("  (hasClassScope %s)" % str(clsvar).lower())
            self.write("  (isStub false)")
            self.write("  (parentType (ref: %s))" % self.ref_mgr.ensure_entity(instance.scope(), False))
        self.write(')')

    def visit_assignname(self, node):
        if node.name == "self" and not isinstance(node.scope(), FunctionDef):
            return
        self._variable(node)
        # In Python, defining a variable is done by assigning
        # something to it. Thus, every FAMIX.LocalVariable should be
        # associated with a corresponding FAMIX.ACCESS with
        # isWrite=true."
        if isinstance(node.parent, Arguments):
            # but do not create an Access node for function arguments
            return
        ref = self.ref_mgr.get_declaration_entity(node)
        refid = self.ref_mgr.ensure_entity(ref, False)
        scopeid = self.ref_mgr.ensure_entity(node.scope(), False)
        # we create an ID for the Access FAMIX element, since it
        # cannot be linked to a unique AST node
        nodeid = self.ref_mgr.new_id()
        self.write('(FAMIX.Access (id: %s)' % nodeid)
        self.write('  (isWrite true)')
        self.write('  (variable (ref: %s))' % refid)
        self.write('  (accessor (ref: %s))' % scopeid)
        self.write(')')
        

    def visit_attribute(self, node):
        try:
            # XXX should handle multiple inferred cases
            for var in node.expr.infer():
                try:
                    attrs = var.getattr(node.attrname)
                    if attrs is YES:
                        continue
                    attr = attrs[0]  # XXX why only the first?
                    break
                except astroid.exceptions.NotFoundError:
                    continue
            else:
                # XXX don't know what to do if we can't infer this access
                return
        except InferenceError:
            return
        if isinstance(var, Module) and not attr in self.ref_mgr:
            self.write('(FAMIX.LocalVariable (id: %s)' % self.ref_mgr.ensure_entity(attr))
            self.write("   (name '%s')" % node.attrname)
            self.write('   (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(var, False))
            self.write(')')
        else:
            ref = self.ref_mgr.get_declaration_entity(attr)
            refid = self.ref_mgr.ensure_entity(ref, False)
            scopeid = self.ref_mgr.ensure_entity(node.scope(), False)
            nodeid = self.ref_mgr.ensure_entity(node)

            self.write('(FAMIX.Access (id: %s)' % nodeid)
            self.write('  (isWrite false)')
            self.write('  (variable (ref: %s))' % refid)
            self.write('  (accessor (ref: %s))' % scopeid)
            self.write(')')

    def visit_name(self, node):
        ref = self.ref_mgr.get_declaration_entity(node)
        if isinstance(ref, tuple):
            # ref is a couple (name, entity) for imported names
            if isinstance(ref[0], six.string_types):
                refname = ref[1].name
                ref = ref[1]
            # or a couple (entity, name) for varargs stuff (ghee)
            else:
                refname = ref[1]
        else:
            refname = ref.name
        if refname == 'self':
            # we don't do anythig when we access 'self'
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        refid = self.ref_mgr.ensure_entity(ref, False)
        scopeid = self.ref_mgr.ensure_entity(node.scope(), False)
        self.write('(FAMIX.Access (id: %s)' % nodeid)
        self.write('  (isWrite false)')
        self.write('  (variable (ref: %s))' % refid)
        self.write('  (accessor (ref: %s))' % scopeid)
        self.write(')')
        try:
            targets = node.infered()
            if targets:
                nodeid = self.ref_mgr.new_id()
                sourceid = self.ref_mgr.ensure_entity(node.scope(), False)
                self.write('(FAMIX.PyReference (id: {})'.format(nodeid))
                self.write('  (source (ref: {}))'.format(sourceid))

                self.write('  (targetCandidates {})'.format(' '.join('(ref: {})'.format(self.ref_mgr.ensure_entity(target, False)) for target in targets)))
                self.write(')')
        except InferenceError:
            pass

    def write_source_anchor(self, node):
        if node.fromlineno is None or node.tolineno is None:
            return
        sourceid = self.ref_mgr.new_id()
        self.write('(FAMIX.PyFileAnchor (id: %s)' % sourceid)
        self.write('  (startLine %d)' % (node.fromlineno))
        self.write('  (endLine %d)' % (node.tolineno))
        self.write('  (element (ref: %d))' % (self.ref_mgr[node]))
        def findfile(node):
            if node is None:
                return None
            if hasattr(node, 'file'):
                return node.file
            return findfile(node.parent)
        self.write("  (fileName '%s')" % findfile(node))
        self.write(')')
        return sourceid

    def write_comment(self, node, id):
        self.write('(FAMIX.Comment (id: %s)' % self.ref_mgr.new_id())
        self.write("  (content '%s')" % node.doc.replace("'", "''"))
        self.write("  (container (ref: %d))" % (id))
        self.write(')')

    def write_source_anchorRef(self, fileid):
        if fileid is not None:
            return self.write("  (sourceAnchor (ref: %s))" % (fileid))

    def _create_import(self, lname, name, node, scoperef):
        try:
            if node is None:
                raise InferenceError()
            mod = node.do_import_module(name)
        except InferenceError:
            # module unavailable
            mod = 'stub<PyModule:{0}>'.format(name)
            if mod not in self.ref_mgr:
                ref = self.ref_mgr.ensure_entity(mod)
                self.write('(FAMIX.PyModule (id: %s)' % ref)
                self.write("  (name '%s')" % name.split('.')[-1])
                self.write('  (isStub true)')
                self.write(')')
            else:
                ref = self.ref_mgr[mod]
        else:
            mod.accept(self) # XXX
            ref = self.ref_mgr.get(mod)

        nodeid = self.ref_mgr.new_id()
        self.write('(FAMIX.PyModuleImport (id: %s)' % nodeid)
        self.write('   (name %r)' % lname)
        self.write('   (importedModule (ref: %s))' % ref)
        self.write('   (parentBehaviouralEntity (ref: %s))' % scoperef)
        self.write(')')
        return mod

    def _create_from(self, lname, name, node):
        reftype = 'importedEntity'
        try:
            mod = node.do_import_module()
        except InferenceError as e:
            # module unavailable
            mod = 'stub<PyModule:{0}>'.format(node.modname)
            if mod not in self.ref_mgr:
                refmod = self.ref_mgr.ensure_entity(mod)
                self.write('(FAMIX.PyModule (id: %s)' % refmod)
                self.write("  (name '%s')" % node.modname)
                self.write('  (isStub true)')
                self.write(')')
            else:
                refmod = self.ref_mgr[mod]
        else:
            mod.accept(self)
            if name == "*":
                impnames = mod.wildcard_import_names()
                for name in impnames:
                    self._create_from(name, name, node)
                return
                    
            else:
                try:
                    # XXX handle candidates properly
                    entity = mod.getattr(name)[0]
                    ref = self.ref_mgr.get_declaration_entity(entity)
                    ref = self.ref_mgr.ensure_entity(ref, False)
                except NotFoundError:
                    # probably some dynamically created objects...
                    # consider these as stub
                    refmod = self.ref_mgr[mod]
                    impname = 'stub<PyImportedName:[{0}]{1}.{2}>'.format(refmod, node.modname, name)
                    if impname not in self.ref_mgr:
                        ref = self.ref_mgr.ensure_entity(impname)
                        self.write('(FAMIX.PyImportedName (id: %s)' % ref)
                        self.write("  (name '%s')" % name)
                        self.write('  (definingModule (ref: %s))' % refmod)
                        self.write(')')
                    else:
                        ref = self.ref_mgr[impname]

        if not isinstance(mod, Module):
            try:
                mod = node.do_import_module(name)
            except InferenceError as e:
                # module unavailable
                mod = 'stub<PyModule:{0}>'.format(node.modname)
                if mod not in self.ref_mgr:
                    refmod = self.ref_mgr.ensure_entity(mod)
                    self.write('(FAMIX.PyModule (id: %s)' % refmod)
                    self.write("  (name '%s')" % node.modname)
                    self.write('  (isStub true)')
                    self.write(')')
                else:
                    refmod = self.ref_mgr[mod]
                impname = 'stub<PyImportedName:[{0}]{1}.{2}>'.format(refmod, node.modname, name)
                if impname not in self.ref_mgr:
                    ref = self.ref_mgr.ensure_entity(impname)
                    self.write('(FAMIX.PyImportedName (id: %s)' % ref)
                    self.write("  (name '%s')" % name)
                    self.write('  (definingModule (ref: %s))' % refmod)
                    self.write(')')
                else:
                    ref = self.ref_mgr[impname]
            else:
                try:
                    mod = node.do_import_module(name)
                except InferenceError as e:
                    raise # XXX what should we do here?
                else:
                    mod.accept(self) # XXX
                    ref = self.ref_mgr.ensure_entity(mod, False)

        nodeid = self.ref_mgr.new_id()
        self.write('(FAMIX.PyNameImport (id: %s)' % nodeid)
        self.write('   (name %r)' % lname)
        self.write('   (%s (ref: %s))' % (reftype, ref))
        self.write('   (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(node.scope(), False))
        self.write(')')


    def visit_import(self, node):
        # an import is cnosidered as a variable assignment of a local
        # variable that references a Module object

        for name, localname in node.names:
            if '.' in name:
                names = name.split('.')
                lnames = names[:]
                if localname is not None:
                    lnames[-1] = localname

                scoperef = self.ref_mgr.ensure_entity(node.scope(), False)
                for i, lname in enumerate(lnames):
                    mod = self._create_import(lname, '.'.join(names[:i+1]), node, scoperef)
                    scoperef = self.ref_mgr.get(mod)
            else:
                if localname is None:
                    lname = name
                else:
                    lname = localname
                self._create_import(lname, name, node, self.ref_mgr.ensure_entity(node.scope(), False))


    def visit_importfrom(self, node):
        for name, localname in node.names:
            if localname is None:
                if '.' in name:
                    lname = name.split('.')[0]
                else:
                    lname = name
            else:
                lname = localname
            self._create_from(lname, name, node)


    def visit_subscript(self, node):
        if self.ref_mgr.is_managed(node):
            return

    def visit_yes(self, node):
        return

    # utility methods
    def _variable(self, node):
        decl = self.ref_mgr.get_declaration_entity(node)
        if decl is not None and decl is not node:
            # do not create a new Attribute or Variable if the node is
            # a new assignment for an already known variable
            return
        if self.ref_mgr.is_managed(node):
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        if decl:
            scope = decl.scope()
        else:
            scope = node.scope()
        if isinstance(scope, ClassDef):
            self.write('(FAMIX.Attribute (id: %s)' % nodeid)
            self.write("  (name '%s')" % node.name)
            self.write("  (hasClassScope true)")
            self.write("  (isStub false)")
            self.write("  (parentType (ref: %s))" % self.ref_mgr.ensure_entity(scope, False))
            self.write(')')
        elif isinstance(scope, Module):
            self.write('(FAMIX.LocalVariable (id: %s)' % nodeid)
            self.write("  (name '%s')" % node.name)
            self.write("  (isStub false)")
            self.write("  (parentBehaviouralEntity (ref: %s))" % self.ref_mgr.ensure_entity(scope, False))
            self.write(')')
        elif isinstance(scope, (FunctionDef, Lambda, GenExpr, DictComp, SetComp)):
            if isinstance(node.parent, Arguments):
                self.write('(FAMIX.PyParameter (id: %s )' % nodeid)
                self.write('  (name %r)' % node.name)
                self.write('  (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(scope, False))
                self.write(')')
            else:
                self.write('(FAMIX.LocalVariable (id: %s)' % nodeid)
                self.write("  (name '%s')" % node.name)
                self.write("  (isStub false)")
                self.write("  (parentBehaviouralEntity (ref: %s))" % self.ref_mgr.ensure_entity(scope, False))
                self.write(')')
        else:
            raise ValueError("Don't know how to convert {}".format(scope))
        return nodeid

    def _has_comment(self, node):
        return node.doc not in (None, '')
