# pylint: disable=missing-docstring

import os
from astroid.node_classes import (AssignAttr, AssignName, Name, Arguments,
                                  ImportFrom, Import, BinOp)
from astroid.scoped_nodes import Module
from astroid.util import YES

from astroid.exceptions import InferenceError

def findfile(node):
    if node is None or node is YES:
        return None
    if hasattr(node, 'file'):
        return node.file
    return findfile(node.parent)


class MSEReferenceManager(dict):
    # pylint: disable=too-many-public-methods
    """Manager for keeping track of AST node references when building
    MSE structure"""
    def __init__(self):
        super(MSEReferenceManager, self).__init__()
        self.idx = 0
        self.managed = set()
        self.roots = set()

    def add_root(self, path):
        path = os.path.abspath(os.path.normpath(path))
        if os.path.isfile(path):
            path = os.path.dirname(path)
        self.roots.add(path)

    def is_stub(self, node):
        file = findfile(node)
        if file:
            file = os.path.abspath(os.path.normpath(file))
            for root in self.roots:
                if file.startswith(root):
                    return False
        return True

    def get_declaration_entity(self, entity):
        decl = self._get_declaration_entity(entity)
        if isinstance(decl, Arguments) and isinstance(entity, (Name, AssignName)):
            if entity.name == decl.vararg:
                return (decl, 'vararg')
            if entity.name == decl.kwarg:
                return (decl, 'kwarg')
        if hasattr(entity, 'name') and isinstance(decl, (ImportFrom, Import)):
            if (entity.name, decl.scope()) in self:
                return (entity.name, decl.scope())
        return decl

    def _get_declaration_entity(self, entity):
        if isinstance(entity, AssignAttr):
            try:
                objs = entity.expr.inferred()
            except InferenceError:
                # no chance we can do something...
                return None
            decl = []
            for obj in objs:
                if obj is YES:
                    continue
                for decl in obj.locals.get(entity.attrname, ()):
                    if self.is_managed(decl):
                        return decl
            # previous and probably wrong implementation...
            scope = entity.scope()
            decl = [node for node in self
                    if isinstance(node, entity.__class__) and
                    node.scope() is scope and
                    entity.attrname == node.attrname]
            if decl:
                assert len(decl) == 1
                return decl[0]
            return entity

        if isinstance(entity, (AssignName,)) and entity.name in entity.scope().locals:
            # got a local variable in the scope, and this node is an
            # affectation, thus it's a local variable, return it
            return entity.scope().locals[entity.name][0]
            # XXX when do we have more than one element in this list?

        if isinstance(entity, (AssignName, Name)):
            scope, candidates = entity.lookup(entity.name)
            if candidates:
                if isinstance(candidates[0], (ImportFrom, Import)):
                    return entity.name, scope
                else:
                    return candidates[0]
            return entity
        if entity in self:# and self[entity] in self.managed:
            return entity

    def get_entity(self, mseid):
        """Return the entity from its mseid"""
        for entity, eid in self.items():
            if mseid == eid:
                return entity

    def find_module(self, name):
        """Look for the Module named 'name' and return its eid"""
        for entity, eid in self.items():
            if isinstance(entity, Module) and entity.name == name:
                return eid

    def ensure_entity(self, entity, managed=True):
        """add an entity

        affect a mseid to the entity (is needed)
        return the mseid affected to the entity"""
        if not isinstance(entity, Name):
            decl = self.get_declaration_entity(entity)
            if decl is not None and decl is not entity:
                # if the node is a new assignement node for an already
                # declared variable in a scope, return the id of the
                # already known (declaration) node
                return self[decl]

        if entity not in self:
            self[entity] = self.new_id()
        if managed:
            assert self[entity] not in self.managed, (self[entity], entity)
            self.managed.add(self[entity])
        return self[entity]

    def is_managed(self, entity):
        return entity in self and self[entity] in self.managed

    def new_id(self):
        self.idx += 1
        return self.idx
    new_Id = new_id

    @staticmethod
    def format(value):
        if value is None:
            return "'None'"
        if isinstance(value, basestring):
            return "'%s'" % (value.replace("'", "''"))
        if isinstance(value, bool):
            return str(value).lower()
        return str(value)
