import compiler
import sys
import os, os.path as osp

from astroid import MANAGER

def build_ASTs(paths):
    result = []
    for path in paths:
        for pyfile in find_py_files(path):
            pydir = osp.dirname(pyfile)
            if pydir not in sys.path:
                sys.path.append(pydir)
            modname = osp.splitext(osp.basename(pyfile))[0]
            result.append((pyfile, MANAGER.ast_from_module_name(modname)))
    return result

def find_py_files(rootpath):
    if osp.isfile(rootpath) and rootpath.endswith('.py'):
        yield rootpath
    else:
        for root, dirs, files in os.walk(rootpath, topdown=True):
            for filename in files:
                if filename.endswith('.py'):
                    yield osp.join(root, filename)
