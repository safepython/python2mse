"""Python to Moose (MSE) converter

It uses astroid to build an AST from Python source code, then it
annotates this AST and generates the MSE file by visiting it.

Main modules are:

* main: provides a main function named doit()

* mseparser: a MSE file parser, used for unit tests mainly

* definitionvisitor: the main visitor to convert annotated AST to MSE file

* complexity: a simple AST annotator that computes the cyclomatic complexity
  of the code

Other modules are utility modules.
"""
